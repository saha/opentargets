import torch
from pytorch_transformers import BertTokenizer
from typing import List, Tuple, Dict, Any
from itertools import chain
from torch.utils.data import Dataset
from torchtext.legacy.data import NestedField, Field
import logging
import os
import pickle
import csv
from nltk.tokenize import WordPunctTokenizer

logger = logging.getLogger(__name__)
# logger.setLevel('INFO')


def load_epmc_dataset(time_dir, data_path="epmc_dataset", model_name='bert-base-cased', stride=64, window_size=128, params_path=None):
    def parse_epmc(data_path, targets=None):
        X = []
        y = []

        X_sent = []
        y_sent = []
        with open(data_path, 'r') as fp:
            csv_reader = csv.reader(fp, delimiter='\t')
            for line in csv_reader:
                if line:
                    token, tag = line[0], line[-1]
                    X_sent.append(token)
                    if targets:
                        if tag.split('-')[-1] in set(targets):
                            y_sent.append(tag)
                        else:
                            y_sent.append('O')
                    else:
                        y_sent.append(tag)
                else:
                    # we reach the end of a sentence
                    if len(X_sent) > 0:
                        X.append(X_sent)
                        y.append(y_sent)
                    X_sent = []
                    y_sent = []
        return X, y

    epmcDataset = {}
    params = None
    for fn in ['train', 'dev', 'test']:
        logger.debug(fn)
        X, y = parse_epmc(os.path.join(data_path, f"{fn}.csv"))
        # print(len(X), len(y))
        # print(sorted(Counter([len(x) for x in X]).items()))
        if fn == 'test':
            with open(os.path.join(time_dir, 'input.pickle'), 'wb') as fi, \
                    open(os.path.join(time_dir, 'tags.pickle'), 'wb') as ft:
                pickle.dump(X, fi)
                pickle.dump(y, ft)
        if fn == 'train':
            if params_path:
                params = pickle.load(open(params_path, 'rb'))
                dataset = NERDatasetBatch.from_params(inputs=X, tags=y, params=params)
            else:
                dataset = NERDatasetBatch(model_name=model_name, inputs=X, tags=y, stride=stride, window_size=window_size)
                params = dataset.get_params()
            with open(os.path.join(time_dir, 'params.pickle'), 'wb') as f:
                pickle.dump(params, f)
        else:
            dataset = NERDatasetBatch.from_params(inputs=X, tags=y, params=params)
        logger.debug(dataset.get_params())
        epmcDataset[fn] = dataset

    for fn in epmcDataset:
        logger.info(f"max bert token length in {fn}: {epmcDataset[fn].max_bert_token_len}")

    return epmcDataset


def load_dataset(data_path="epmc_dataset", params_path=None):
    def parse_epmc(data_path, targets=None):
        X = []
        y = []

        X_sent = []
        y_sent = []
        with open(data_path, 'r') as fp:
            for line in fp:
                if line.strip():
                    row_data = line.split('\t')
                    if len(row_data) != 2:
                        raise ValueError(f'expect 2 columns in the file but got {len(row_data)}')
                    token, tag = row_data[0], row_data[-1]
                    X_sent.append(token)
                    if targets:
                        if tag.split('-')[-1] in set(targets):
                            y_sent.append(tag)
                        else:
                            y_sent.append('O')
                    else:
                        y_sent.append(tag)
                else:
                    # we reach the end of a sentence
                    if len(X_sent) > 0:
                        X.append(X_sent)
                        y.append(y_sent)
                    X_sent = []
                    y_sent = []
        return X, y

    X, y = parse_epmc(data_path)

    params = pickle.load(open(params_path, 'rb'))
    dataset = NERDatasetBatch.from_params(inputs=X, tags=y, params=params)

    logger.debug(dataset.get_params())

    return dataset


def load_csv_dataset_tokenize(data_path="epmc_dataset", params_path=None):
    tokenizer = WordPunctTokenizer()

    def parse_epmc(data_path):
        X = []
        y = []
        with open(data_path, 'r') as fp:
            reader = csv.reader(fp, delimiter='\t')
            for row_data in reader:
                if len(row_data) != 4:
                    print(row_data)
                    raise ValueError(f'expect 4 columns in the file but got {len(row_data)}')
                sent = row_data[2]
                tokens = tokenizer.tokenize(sent)
                X.append(tokens)
        return X, y

    X, y = parse_epmc(data_path)

    params = pickle.load(open(params_path, 'rb'))
    dataset = NERDatasetBatch.from_params(inputs=X, tags=y, params=params)

    logger.debug(dataset.get_params())

    return dataset


class NERDatasetBatch(Dataset):
    def __init__(self, model_name: str, inputs: List[List[str]], tags: List[List[str]] = None,
                 window_size: int = 128, stride: int = 64,
                 label2idx: Dict[str, int] = None, idx2label: List[str] = None,
                 max_bert_token_len: int = None, max_ner_token_len: int = None):
        if 'uncased' not in model_name:
            lower = False
        else:
            lower = True
        self.model_name = model_name
        self.tokenizer = BertTokenizer.from_pretrained(model_name, do_lower_case=lower)

        self.inputs = inputs
        self.tags = tags
        self.window_size = window_size
        self.stride = stride

        if max_ner_token_len:
            self.max_ner_token_len = max_ner_token_len
        else:
            # max ner token length doesn't include [CLS] and [SEP]
            self.max_ner_token_len = self._get_max_ner_len(inputs=inputs)

        if max_bert_token_len:
            self.max_bert_token_len = max_bert_token_len
        else:
            self.max_bert_token_len = self._get_max_bert_len(inputs=inputs)

        if self.max_ner_token_len > 512:
            print(f"expects maximum sequence length 512 but got {self.max_ner_token_len} tokens")
        else:
            if self.max_bert_token_len > 512:
                print(f"Bert expects maximum sequence length 512 but got {self.max_bert_token_len} bert tokens")

        if (not label2idx or not idx2label) and tags:
            self.label2idx, self.idx2label = self.init_label_dict(tags)
        else:
            self.label2idx = label2idx
            self.idx2label = idx2label

    def slide_sent(self, sent, idx):
        start_offset = 0
        windows = []
        split_alignment = []
        split = False

        if len(sent) > 510:
            split = True
        elif len(sent) < self.window_size:
            split = False
        else:
            bert_len = 0
            for token in sent:
                bert_len += len(self.tokenizer.tokenize(token))
                if bert_len > 510:
                    split = True

        # since later we need to add [CLS] and [SEP] which will take up 2 spaces
        if split:
            while start_offset < len(sent):
                length = len(sent) - start_offset
                if length > self.window_size:
                    length = self.window_size
                split_alignment.append(idx + len(windows))
                windows.append(sent[start_offset: start_offset + length])
                if start_offset + length == len(sent):
                    break
                start_offset += min(length, self.stride)
        else:
            split_alignment.append(idx+len(windows))
            windows.append(sent)

        return windows, split_alignment, len(sent)

    def split_inputs(self, inputs):
        # check whetehr we need to split sentences
        max_len = max([len(s) for s in inputs])
        if max_len <= self.window_size:
            return inputs, None, None

        split_inputs = []
        split_alignments = []
        input_lengths = []
        offset = 0

        for idx, sent in enumerate(inputs):
            off_idx = idx + offset
            windows, split_align, length = self.slide_sent(sent, idx=off_idx)
            split_inputs.extend(windows)
            split_alignments.append(split_align)
            input_lengths.append(length)
            if len(windows) > 1:
                offset += len(windows) - 1
        #check whether split happens
        return split_inputs, split_alignments, input_lengths

    def get_token_mask(self, inputs):
        max_len = max([len(s) for s in inputs])
        token_mask = []
        for sent in inputs:
            sent_len = len(sent)
            token_mask.append([1]*sent_len + [0]*(max_len-sent_len))
        return token_mask

    def tokens_totensor(self, inputs):
        """pad"""
        token_mask = self.get_token_mask(inputs)
        inputs, split_alignments, lengths = self.split_inputs(inputs)
        encoded_sents, bert_token_mask, wordpiece_alignment = self.encode_sents(inputs=inputs)
        padded_sequences, attention_mask = self.pad_token_sequences(sequences=encoded_sents)
        return torch.tensor(padded_sequences), torch.tensor(attention_mask).bool(), \
               torch.tensor(bert_token_mask).bool(), wordpiece_alignment, \
               split_alignments, lengths, torch.tensor(token_mask).bool()

    def tags_totensor(self, tags):
        """pad"""
        tag_ids = self.encode_labels(tags)
        return torch.tensor(self.pad_tag_sequences(tag_sequences=tag_ids))

    def tag_stats(self):
        """get stats of tags"""
        tag_set = self.label2idx.keys()
        num_tags = len(self.label2idx)
        return num_tags, tag_set

    def init_label_dict(self, tags):
        label2idx = {'[PAD]': 0}
        idx2label = ['[PAD]']

        labels = set(list(chain.from_iterable(tags)))
        for l in labels:
            label2idx[l] = len(label2idx)
            idx2label.append(l)
        return label2idx, idx2label

    def _get_max_ner_len(self, inputs: List[List[str]]) -> int:
        """get maximum token length of original dataset"""
        return max([len(x) for x in inputs])

    def _get_max_bert_len(self, inputs):
        max_len = 0
        for sent in inputs:
            length = 0
            for token in sent:
                length += len(self.tokenizer.tokenize(token))
            if length > max_len:
                max_len = length
        return max_len

    def pad_token_sequences(self, sequences: List[List[int]]) -> Tuple[List, List]:
        """
        pad the bert encoded sequences
        :param sequences:
        :type sequences:
        :param pad_ner:
        :type pad_ner:
        :return:
        :rtype:
        """
        padded_sequences = []
        attention_mask = []
        max_len = max([len(seq) for seq in sequences])
        for seq in sequences:
            padded_seq = seq + [0] * (max_len - len(seq))
            padded_sequences.append(padded_seq)
            attention_mask.append(list(map(lambda x: 1 if x > 0 else 0, padded_seq)))
        return padded_sequences, attention_mask

    def pad_tag_sequences(self, tag_sequences):
        padded_sequences = []
        max_len = max([len(seq) for seq in tag_sequences])

        for seq in tag_sequences:
            padded_seq = seq + [0] * (max_len - len(seq))
            padded_sequences.append(padded_seq)
        return padded_sequences

    def encode_sents(self, inputs: List[List[str]]) -> Tuple[List, List, List]:
        """
        encode string to ids and keep track the alignment
        :param inputs:
        :type inputs:
        :return:
        :rtype:
        """
        encoded_sents = []
        wordpiece_alignment = []
        token_mask = []
        # max length of the original sentence
        max_len = max([len(seq) for seq in inputs])
        logger.debug(f"max length in current batch: {max_len}")
        inputs = self._truncate_sequences(inputs)
        for sent in inputs:
            sent_encodes, alignment, mask = self._encode_sent(sent, max_len=max_len)
            encoded_sents.append(sent_encodes)
            wordpiece_alignment.append(alignment)
            token_mask.append(mask)
            # print(len(sent), self.max_ner_token_len)
        return encoded_sents, token_mask, wordpiece_alignment

    def _encode_sent(self, input: List[str], max_len: int) -> Tuple[List, List, List]:
        """
        encode a single sentence
        :param input:
        :type input:
        :return:
        :rtype:
        """
        encoded_sent = []
        alignment = []
        token_mask = []
        input = ['[CLS]'] + input + ['[SEP]']
        try:
            for i, token in enumerate(input):
                # skip [CLS] and [SEP] to only keep track alignment of tokens
                if token not in ('[CLS]', '[SEP]'):
                    alignment.append(len(encoded_sent))
                    token_mask.append(1)
                encoded_sent.extend(self.tokenizer.encode(token))
            token_mask += [0] * (max_len - len(token_mask))
            if len(encoded_sent) > 512:
                logger.debug(input)
                raise ValueError(f"expect maximum 512 bert tokens but got {len(encoded_sent)}")
        except Exception as e:
            logger.info(e)
            encoded_sent = []
            alignment = []
            token_mask = []
            input = ['[CLS]'] + input + ['[SEP]']
            for i, token in enumerate(input):
                # skip [CLS] and [SEP] to only keep track alignment of tokens
                if token not in ('[CLS]', '[SEP]'):
                    alignment.append(len(encoded_sent))
                    token_mask.append(1)
                encoded_sent.extend(self.tokenizer.encode(token)[:2])
            token_mask += [0] * (max_len - len(token_mask))
            if len(encoded_sent) > 512:
                logger.debug(input)
                raise ValueError(f"expect maximum 512 bert tokens but got {len(encoded_sent)}")

        return encoded_sent, alignment, token_mask

    def _truncate_sequences(self, sequences):
        """
        truncate the sequences if we want training/evaluation/test data to have the sequence length
        :param sequences:
        :type sequences:
        :return:
        :rtype:
        """
        if self.max_ner_token_len == -1:
            return sequences
        else:
            return [seq[:self.max_ner_token_len] for seq in sequences]

    def encode_labels(self, tags):
        """
        encode labels to nums
        :param tags:
        :type tags:
        :return:
        :rtype:
        """
        tags = self._truncate_sequences(tags)
        label_encoding = []

        for sent_tags in tags:
            label_encoding.append([self.label2idx[t] for t in sent_tags])
        return label_encoding

    def get_params(self):
        """get params"""
        params = {'model_name': self.model_name,
                  'label2idx': self.label2idx,
                  'idx2label': self.idx2label,
                  'max_bert_token_len': self.max_bert_token_len,
                  'max_ner_token_len': self.max_ner_token_len,
                  'num_tags': self.tag_stats()[0],
                  'window_size': self.window_size,
                  'stride': self.stride}
        return params

    @classmethod
    def from_params(cls, inputs: List[List[str]], params: Dict[str, Any], tags: List[List[str]] = None):
        """
        generate class from params

        :param inputs:
        :type inputs:
        :param tags:
        :type tags:
        :param params:
        :type params:
        :return:
        :rtype:
        """
        model_name = params['model_name']
        label2idx = params['label2idx']
        idx2label = params['idx2label']
        max_bert_token_len = params['max_bert_token_len']
        max_ner_token_len = params['max_ner_token_len']

        # max_bert_token_len = None
        # max_ner_token_len = None
        window_size = params['window_size']
        stride = params['stride']

        return cls(model_name=model_name, inputs=inputs, tags=tags,
                   label2idx=label2idx, idx2label=idx2label,
                   max_bert_token_len=max_bert_token_len,
                   max_ner_token_len=max_ner_token_len,
                   window_size=window_size,
                   stride=stride)

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        inputs = self.inputs[idx]

        sample = {'input': inputs}
        if self.tags:
            tag = self.tags[idx]
            sample.update({'tag': tag})
        return sample

