import numpy as np
import matplotlib.pyplot as plt
import logging
import requests
import requests_cache

requests_cache.install_cache('ground_cache')

logger = logging.getLogger(__name__)


class EarlyStopp(object):
    def __init__(self, threshold=5):
        self.threshod = threshold
        self.scores = []
        # performance decrease
        self.consecutive_decrease = 0

    def stop_training(self, loss, epoch):
        '''
        :param score: score on validation set
        :param step: iteration step
        :return: boolean
        '''
        self.scores.append(loss)

        if len(self.scores) > 1 and self.scores[-1] > self.scores[-2]:
            self.consecutive_decrease += 1
        else:
            self.consecutive_decrease = 0
        # print self.consecutive_decrease, self.scores
        if self.consecutive_decrease >= self.threshod:
            logger.info("Early stop criteria {} reached! Stopped at epoch: {}".format(self.threshod, epoch))
            return True
        else:
            return False


def plot_transitions(transitions, tags, cmap=plt.cm.Blues):
    fig, ax = plt.subplots()
    im = ax.imshow(transitions, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(transitions.shape[1]),
           yticks=np.arange(transitions.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=tags, yticklabels=tags,
           title='transition matrix',
           ylabel='From',
           xlabel='To')

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(transitions.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(transitions.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    # Loop over data dimensions and create text annotations.
    fmt = '.2f'
    thresh = transitions.max() / 2.
    for i in range(transitions.shape[0]):
        for j in range(transitions.shape[1]):
            ax.text(j, i, format(transitions[i, j], fmt),
                    ha="center", va="center",
                    color="white" if transitions[i, j] > thresh else "black")
    fig.tight_layout()
    return ax, fig


def my_collate(batch):
    """
    customised collate function to make it possible to deal with varied alignment list
    because default collate function deals with tensors
    :param batch:
    :type batch:
    :return:
    :rtype:
    """
    inputs = []
    tags = []
    for sample in batch:
        inputs.append(sample['input'])
        if 'tag' in sample:
            tags.append(sample['tag'])

    samples = {'input': inputs}
    # print("size...")
    if tags:
        samples.update({'tag': tags})
    return samples


def modify_labels(labels):
    new_labs = []
    for labs in labels:
        prev = 'O'
        new_sent = []
        for l in labs:
            if len(prev.split('-')) > 1:
                prev_pos, prev_tag = prev.split('-')
            else:
                prev_pos = 'O'
                prev_tag = 'O'

            if len(l.split('-')) > 1:
                curr_pos, curr_tag = l.split('-')
            else:
                curr_pos = 'O'
                curr_tag = 'O'

            if prev_pos == 'O':
                if curr_pos == 'I':
                    l = l.replace('I-', 'B-')

            if prev_pos == 'B' or prev_pos == 'I':
                if curr_pos == 'I' and curr_tag != prev_tag:
                    l = l.replace('I-', 'B-')
            prev = l

            new_sent.append(l)
        new_labs.append(new_sent)
    return new_labs


def ground_annotation(exact, entity):
    grounded_value = 'not-grounded'
    root_url = 'https://www.ebi.ac.uk/ols/api/search?'
    if entity == 'DS':
        URL = root_url+'q='+exact+'&exact=false&ontology=efo'
    elif entity == 'OG':
        URL = root_url+'q='+exact+'&exact=false&ontology=ncbitaxon'
    else:
        URL = 'http://10.7.35.118:8157/solr/Genes/select?q='+exact

    r = requests.get(url=URL)
    if r.status_code == 200:
        json_data = r.json()
    else:
        return grounded_value
    if json_data['response']['numFound'] != 0:
        if entity in ('DS', 'OG'):
            grounded_value = json_data['response']['docs'][0]['iri']
        elif entity in ('GP',):
            grounded_value = json_data['response']['docs'][0]['ID']
    return grounded_value


if __name__ == "__main__":
    print(ground_annotation('brca5', 'DS'))
