import torch
import torch.nn as nn
from pytorch_transformers import BertModel, BertConfig
from typing import List, Tuple, Dict, Any
from torchtext.legacy.data import NestedField, Field
import logging
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


logger = logging.getLogger(__name__)
# logger.setLevel('DEBUG')


class BertEmbedding(nn.Module):
    def __init__(self, model_name, stride, alignment_type: str = 'first'):
        super().__init__()
        self.stride = stride
        self.bert_dim = 768
        self.alignment_type = alignment_type
        self.bert_config = BertConfig.from_pretrained(model_name)
        self.bert_model = BertModel.from_pretrained(model_name, config=self.bert_config)

    def first_alignment(self, states, alignment, size):
        """
        alignment to take the first embedding of wordpiece token
        :param states:
        :type states:
        :param alignment:
        :type alignment:
        :param size:
        :type size:
        :return:
        :rtype:
        """
        batch_size, token_len = size
        aligned_embedding = torch.zeros(batch_size, token_len, self.bert_dim)
        for i, align in enumerate(alignment):
            ori_indices = list(range(len(align)))
            # print(ori_indices, align)
            aligned_embedding[i, ori_indices] = states[i, align]
        return aligned_embedding

    def embedding_alignment(self, states, alignment, size):
        """
        due to wordpiece tokenization, there may be more tokens than original sequence,
        we need to align them. [CLS] and [SEP] are also removed to only keep embeddings of input tokens.
        The output is the aligned embedding which has same length (i.e. max_ner_token_len) of input sequences.
        :param states:
        :type states:
        :param alignment:
        :type alignment:
        :param size:
        :type size:
        :return:
        :rtype:
        """
        if self.alignment_type == 'first':
            aligned_embedding = self.first_alignment(states, alignment, size)
        elif self.alignment_type == 'last':
            aligned_embedding = None
        elif self.alignment_type == 'avg':
            aligned_embedding = None
        else:
            raise ValueError
        return aligned_embedding

    def merge_split(self, embedding, splits, token_mask):

        # embedding = torch.randint(1,10,(5,5,7))
        # self.bert_dim = 7
        batch_size, seq_len = (len(splits[1]), max(splits[1]))
        idx_splits, lens_splits = splits

        ner_embedding = torch.zeros(batch_size, seq_len, self.bert_dim)
        for idx, sp in enumerate(idx_splits):
            if isinstance(sp, list):
                bert_embed = embedding[sp]
                token_embed = bert_embed[token_mask[sp]]
                orig_embed = torch.zeros(len(sp), lens_splits[idx], token_embed.size(1))
                orig_mask = torch.zeros(orig_embed.size(0), orig_embed.size(1))
                # print(bert_embed)
                # print("token embedding")
                # print(token_embed)
                # print(orig_embed.size())
                # print(token_mask[sp].size())
                prev_offset = 0
                for edx, mask in enumerate(token_mask[sp]):
                    num_of_tokens = mask.sum()
                    start_offset = edx * self.stride
                    orig_embed[edx, start_offset:start_offset+num_of_tokens] = token_embed[prev_offset:prev_offset+num_of_tokens]
                    orig_mask[edx, start_offset:start_offset+num_of_tokens] = token_mask[sp][edx, :num_of_tokens]
                    prev_offset += num_of_tokens

                # print('final embedding')
                # print(bert_embed)
                # print(token_embed)
                # print(orig_embed)
                final_embed = orig_embed.sum(dim=0)
                # print(final_embed)
                # print(final_embed.size())
                # print(orig_mask)
                final_counts = orig_mask.sum(dim=0).float().view(-1, 1)
                # print(final_counts, final_counts.size())
                # print("final count")
                # print(final_counts[:10])
                final_embed = final_embed/final_counts
                # print(final_embed.size())
                # print(final_embed)
            # print("original_mask")
            # print(orig_mask[:, :10])
            # print("final embed")
            # print(final_embed.size())
            # print(final_embed[-1, :10])
            else:
                final_embed = embedding[sp]
            # print(ner_embedding.size(), final_embed.size())
            # print(final_embed.size(0))
            # print(ner_embedding[idx, :final_embed.size(0)].size())
            ner_embedding[idx, :final_embed.size(0)] = final_embed
        #     print("---")
        # print(ner_embedding)

        return ner_embedding

    def forward(self, input_ids, attention_mask, split_token_mask, alignment, splits):
        """
        generate bert embedding

        :param input_ids: bert tokenizer encoded inputs
        :type input_ids:
        :param attention_mask: attention mask
        :type attention_mask:
        :param bert_token_mask: mask to select original ner tokens
        :type bert_token_mask:
        :param alignment: alignment to map bert tokens to original ner tokens
        :type alignment:
        :return:
        :rtype:
        """
        mask_size = split_token_mask.size()

        bert_last_state = self.bert_model(input_ids, attention_mask=attention_mask)[0].to(torch.device('cpu'))

        # print(f'original bert embedding: {bert_last_state.size()}')
        # print(bert_last_state[:, -1, :10])

        aligned_embedding = self.embedding_alignment(bert_last_state, alignment, size=mask_size)
        # print(f'bert embedding after alignment: {aligned_embedding.size()}')
        # print(aligned_embedding[:, -1, :10])
        if not splits[0]:
            bert_embedding = aligned_embedding
        else:
            bert_embedding = self.merge_split(aligned_embedding, splits, split_token_mask)
        # print(bert_embedding[:, -1, :10])
        # print(bert_embedding.size())
        return bert_embedding


class CharEmbedding(nn.Module):
    def __init__(self, char_embed_len, char_embed_dim, padding_idx=1, batch_first=True):
        super().__init__()
        self.char_embed_len = char_embed_len
        self.char_embed_dim = char_embed_dim
        self.char_embedding = nn.Embedding(num_embeddings=char_embed_len,
                                           embedding_dim=char_embed_dim,
                                           padding_idx=padding_idx)
        self.char_encoder = nn.GRU(input_size=char_embed_dim, hidden_size=char_embed_dim, batch_first=batch_first)

    def get_char_embedding(self, char_ids, char_lengths):
        embedding = self.char_embedding(char_ids)
        # print(f"embedding size: {embedding.size()}")

        batch_size, seq_sent, seq_char, char_dim = embedding.size()
        # print(char_ids.size())
        # print(batch_size*seq_sent, seq_char, char_dim)
        # print(char_lengths.view(-1).size())
        packed_embedding = pack_padded_sequence(embedding.view(batch_size * seq_sent, seq_char, char_dim),
                                                char_lengths.view(-1), enforce_sorted=False, batch_first=True)
        out, states = self.char_encoder(packed_embedding)
        # print("----")
        # out, _ = nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # print(states.size())
        # print(f"encoder output size: {out.size()}, last state size: {states[0].size()}")
        h_n = states.view(batch_size, seq_sent, char_dim)
        # print(h_n.size())
        return h_n

    def forward(self, char_ids, char_lengths):
        char_embedding = self.get_char_embedding(char_ids=char_ids, char_lengths=char_lengths).to(torch.device('cpu'))
        return char_embedding
