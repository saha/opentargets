import torch.nn as nn
from biobert.model.crf_model import ConditionalRandomField
from biobert.model.embedding_model import BertEmbedding
from typing import List, Tuple
import logging

logger = logging.getLogger(__name__)
# logger.setLevel('DEBUG')


class BertCRF(nn.Module):
    def __init__(self, num_tags: int, model_name: str,
                 bert_alignment_type: str = 'first', stride: int = 64,
                 constraints: List[Tuple[int, int]] = None,
                 include_start_end_transitions: bool = True):
        super().__init__()
        self.bert_dim = 768
        self.num_tags = num_tags
        self.bert_alignment_type = bert_alignment_type
        self.bert_model = BertEmbedding(model_name=model_name, stride=stride, alignment_type=bert_alignment_type)

        self.crf_model = ConditionalRandomField(num_tags=num_tags,
                                                constraints=constraints,
                                                include_start_end_transitions=include_start_end_transitions)

        self.linear = nn.Linear(self.bert_dim, num_tags)

    def emit_embedding(self, bert_input_ids, attention_mask, token_mask, alignment, splits):
        """
        logits from sequence model

        :param bert_input_ids:
        :type bert_input_ids:
        :param attention_mask:
        :type attention_mask:
        :param alignment:
        :type alignment:
        :param splits:
        :type splits:
        :return:
        :rtype:
        """

        bert_embedding = self.bert_model(input_ids=bert_input_ids,
                                         attention_mask=attention_mask,
                                         split_token_mask=token_mask,
                                         alignment=alignment,
                                         splits=splits)
        # print(bert_embedding[:, -1:, :10])
        # print(bert_embedding.size())
        emits = self.linear(bert_embedding)
        return emits

    def forward(self, input_ids, tag_ids, bert_attention_mask, bert_token_mask, alignment, splits, token_mask):
        """
        combine the sequence model and crf model

        :param input_ids:
        :type input_ids:
        :param tag_ids:
        :type tag_ids:
        :param bert_attention_mask:
        :type bert_attention_mask:
        :param bert_token_mask:
        :type bert_token_mask:
        :param token_mask:
        :type token_mask:
        :param alignment:
        :type alignment:
        :param splits:
        :type splits:
        :return:
        :rtype:
        """
        emits = self.emit_embedding(input_ids, bert_attention_mask, bert_token_mask, alignment, splits)
        # print("emit")
        # print(emits[:, -20:])
        neg_log_likelihood = - self.crf_model(emits, tag_ids, mask=token_mask)
        return neg_log_likelihood

    def predict(self, input_ids, bert_attention_mask, bert_token_mask, alignment, splits, token_mask, tag_ids=None):
        """
        infer the predictions given input
        :param input_ids:
        :type input_ids:
        :param tag_ids:
        :type tag_ids:
        :param bert_attention_mask:
        :type bert_attention_mask:
        :param bert_token_mask:
        :type bert_token_mask:
        :param token_mask:
        :type token_mask:
        :param alignment:
        :type alignment:
        :param splits:
        :type splits:
        :return:
        :rtype:
        """
        emits = self.emit_embedding(input_ids, bert_attention_mask, bert_token_mask, alignment, splits)
        neg_log_likelihood = None
        if tag_ids is not None:
            neg_log_likelihood = - self.crf_model.forward(emits, tag_ids, mask=token_mask)
        # print(neg_log_likelihood)
        preds = self.crf_model.viterbi_tags(emits, mask=token_mask)
        return neg_log_likelihood, preds


class BertCharCRF(nn.Module):
    def __init__(self):
        super().__init__()
        pass
