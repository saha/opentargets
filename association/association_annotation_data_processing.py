# this code reads the GOLD standard dataset in the JSON format and create a TSV file with the association sentences only.

import argparse
import json
import pandas as pd
import random


def process_json(jsonFile='ner_rel_fulltext_full.json', associationFile='associations.tsv'):
    with open(associationFile, 'w') as assocWR:
        assocWR.write('PMCIDSentID\tSentence\tLabel\n')
    with open(jsonFile) as jsonRD:
        jsonObj = json.load(jsonRD)
    with open(associationFile, 'a') as assocWR:    
        for jo in jsonObj.keys():
            # PMCID
            for sent in jsonObj[jo]['annotations']:
                if sent['rel']:
                    print(sent['sent'])
                    print(sent['rel'])
                    assocWR.write('{}\t{}\t{}\n'.format(jo+'_'+str(sent['sid']), sent['sent'], sent['rel']))

           
def getSublist(indexList, train=0.7, dev=0.15, test=0.15):
    ys = list(indexList)
    train_size = round(len(ys) * train)
    dev_size = round(len(ys) * dev)
    test_size = round(len(ys) * test)
    random.shuffle(ys)
    return ys[:train_size], ys[train_size:train_size+dev_size], ys[train_size+dev_size:]


def split_data(filename):
    data = pd.read_csv(filename, sep='\t')
    ygd_idx = data[data['label']=='YGD'].index
    ngd_idx = data[data['label']=='NGD'].index
    amb_idx = data[data['label']=='AMB'].index
    train_ygd, dev_ygd, test_ygd = getSublist(ygd_idx.to_list())
    train_ngd, dev_ngd, test_ngd = getSublist(ngd_idx.to_list())
    train_amb, dev_amb, test_amb = getSublist(amb_idx.to_list())
    return data.iloc[train_ygd + train_ngd + train_amb], data.iloc[dev_ygd + dev_ngd + dev_amb], data.iloc[test_ygd + test_ngd + test_amb]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This script will process Gold Standard in JSON format and extract the association sentences and save in TSV format.')
    parser.add_argument("-f", "--file", nargs=1, required=True, help="Gold Standard in JSON format", metavar="PATH")
    parser.add_argument("-o", "--out", nargs=1, required=True, help="Output file", metavar="PATH")
    
    args = parser.parse_args()
    print(args.out[0])
    process_json(args.file[0], args.out[0])
    train, dev, test = split_data(associationFile)
    train.to_csv('train.tsv', sep='\t')
    dev.to_csv('dev.tsv', sep='\t')
    test.to_csv('test.tsv', sep='\t')
